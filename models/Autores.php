<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $fechaNacimiento
 *
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'string', 'message'=>'La fecha de nacimiento no es válida'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'email'],
            [['fechaNacimiento','nombre','email'], 'required',  'message'=>'rellene todos los datos'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código del Autor',
            'nombre' => 'Nombre completo del Autor',
            'email' => 'Correo electronico',
            'fechaNacimiento' => 'Fecha Nacimiento del Autor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['autor' => 'id']);
    }
    /* metodo nuevo */ 
    public function afterFind(){
        parent::afterFind();
        $this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:d-m-Y');
    }
    
    /* antes de grabar*/
    public function beforeSave($insert){
        parent::beforeSave($insert);
        $this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:Y-m-d');
        return(true);
    }
    
    
}
