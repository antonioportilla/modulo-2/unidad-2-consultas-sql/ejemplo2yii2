<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
      /* ejemplo de enviar un control de una tabla con DAO ************************************************** 
        $tablalibros=yii::$app->db->createCommand('select * from libros')->queryAll();
        echo "<pre>";
        var_dump($tablalibros[0]['autor']);
        exit; */ 
        return $this->render('index');
    }
  
    
      public function actionGestion()
    {
      /* ejemplo de enviar un control de una tabla con DAO ************************************************** 
        $tablalibros=yii::$app->db->createCommand('select * from libros')->queryAll();
        echo "<pre>";
        var_dump($tablalibros[0]['autor']);
        exit; */ 
        return $this->render('panel');
    }
    /*  Creamos a mano una accion para el model de Autores
    
    public function actionAutor(){
        
        $id=50;
        $modelo= \app\models\Autores::find()->where("id=$id")->one();
  
        if($modelo->load(Yii::$app->request->post())){ // si pulso enviar
            $modelo->save();
        }
            
        return $this->render("autores",[
            'model'=>$modelo,
            
            ]); //envia datos a la view
        
    }
     * 
     */
}
