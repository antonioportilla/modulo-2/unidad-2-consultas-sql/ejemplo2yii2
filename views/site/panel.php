<?php

use yii\helpers\Html;

echo Html::a('Autores',
        ['autores/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Libros',
        ['libros/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';




?>